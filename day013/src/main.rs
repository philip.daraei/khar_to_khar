/*
use std::error::Error;
use std::fs;
use std::io;

fn read_username_from_file() -> Result<String, io::Error> {
    fs::read_to_string("username.txt")
}

fn main() -> Result<(), Box<dyn Error>> {
    let username = read_username_from_file()?;
    println!("{}", username);
    Ok(())
}

pub struct Guess {
    value: i32,
}

impl Guess {
    pub fn new(value: i32) -> Guess {
        if value < 1 || value > 100 {
            panic!("Guess value must be between 1 and 100, got {}.", value);
        }

        Guess { value }
    }

    pub fn value(&self) -> i32 {
        self.value
    }
}
*/

fn find_largest<T>(list: &[T]) -> &T {
    let mut largest = list[0];
    for item in list {
        if item > largest {
            largest = item;
        }
    }

    largest
}

fn main() {
    let number_list = vec![34, 50, 100, 65];
    let result = find_largest(&number_list);
    println!("The largest number is {}.", result);

    let number_list = vec![102, 34, 6000, 89, 54, 2, 43, 8];

    let result = find_largest(&number_list);
    println!("The largest number is {}", result);

    let integer_point = Point { x: 5, y: 10 };
    let float = Point { x: 3.0, y: 2.56 };

    let integer_and_float = DifferentGenericPoints { x: 4, y: 10.23 };
}

struct Point<T> {
    x: T,
    y: T,
}

struct DifferentGenericPoints<T, U> {
    x: T,
    y: U,
}
