// Import
// import UIKit

// Variable 
var aaa = 5
var bbb:Int = 15

// Constants
let ABCDE = 20
let CONS = "I NEVER CHANGE"

// Different types
var singleLineString = "Hello world!"
var multiLineString = """
    Que passssaaa,
    what's up?
"""
var boolean = true
var abcd:Double

// String interpolation
var verb = "awesome!"
var str = "You are \(verb)"

// Arrays
var beatles = ["John", "Paul", "Ringo", "George"]
let deadBeatles = beatles[0]

// Sets - only unique values - unordered
var colors = Set(["red", "green", "blue"])

// Enums with associated value
enum Activities {
    case running(distance: Int)
    case sholex
}

// Tuples
var name = (first: "Taylor", last: "Swift")
var _ = name.0
var _ = name.first

// Dictionaries
var heights = [
    "Phil": "1.88",
    "everyone else": "1.60 at best"
]
var _ = heights["Phil"]
// default value for non existing entries are possible height["lil": default: "unknown"]

// Creating empty collections
var emptyArray = [Int]()
var emptyDictionary = [Int: String]()

var emptySet = Set<String>()
var alternativeArray = Array<Int>()
var alternativeDic = Dictionary<Int, String>()

let todaysActivity = Activities.running(Distance: 6)

// Enum raw values
enum Planet: Int {
    case mars = 0
    case earth
    case venus
    case pluto
}

let earth = Planet(rawValue: 1)!

// Arithmetic operators
// + - * / %

// Compound Operators
// += -+ /= *=

// Comparison Operators
// == != >= <= 

// Operator overloading
let meaningOfLife = 42
let doubleMeaning = meaningOfLife + 42

let firstHalf = ["John", "Paul"]
let secondHalf = ["George", "Ringo"]
let newBeatles = firstHalf + secondHalf

// conditions
var phil = true
var hani = false

for _ in 1...2 {
    if phil {
        print("you are cool 😎")
        phil = false
    } else if !hani {
        print("okay okay you are cool too 😎")
    } else {
        print("you not so cool")
    }
}
phil = true

// combining operators
let age1 = 11
let age2 = 12

if age1 > 18 && age2 > 18 {
    print("Both are over 18")
}

if phil || hani {
    print("at least one cool person 😎")
}

// Tenary operator
print(phil ? "Damn boy, you true!" : "Damn boy you false")

// Switch cases
let weather = "rain"

switch weather {
case "sunny":
    print("Wear suncream")
case "snow":
    print("Wrap up warm")
case "rain":
    print("Dance in the rain")
    fallthrough
default:
    print("Enjoy your day")
}

// range operators
// half open range operator
let halfOpen = 1..<5 // 1,2,3,4

// closed range operator
let closedRange = 1...5 //1,2,3,4,5

// MARK: - LOOPS
// For loop
for item in halfOpen {
    print("\(item)")
}

print("Players gonna ")

for _ in closedRange {
    print("play")
}

// While loop
var number = 1

while number <= 20 {
    print(number)
    number += 1
}
