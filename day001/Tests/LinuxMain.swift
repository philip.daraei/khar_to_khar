import XCTest

import day001Tests

var tests = [XCTestCaseEntry]()
tests += day001Tests.allTests()
XCTMain(tests)
