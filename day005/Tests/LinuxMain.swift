import XCTest

import day005Tests

var tests = [XCTestCaseEntry]()
tests += day005Tests.allTests()
XCTMain(tests)
