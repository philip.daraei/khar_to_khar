struct Ipv4Addr {
    // --snip--
}

struct Ipv6Addr {
    // --snip--
}

enum IpAddr {
    V4(Ipv4Addr),
    V6(Ipv6Addr),
}

enum Message {
    Quit,                       // no data associated with it
    Move { x: i32, y: i32 },    // includes an anonymous struct
    Write(String),              // includes a single string
    ChangeColor(i32, i32, i32), // includes 3 values
}

impl Message {
    fn call(&self) {
        // method body would be here defined here
    }
}

fn main() {
    println!("Hello, world!");
    let w = Message::Write(String::from("Hello"));
    let c = Message::ChangeColor(255, 255, 0);
    let m = Message::Move { x: 2, y: 3 };
    let q = Message::Quit;
    w.call()
}
