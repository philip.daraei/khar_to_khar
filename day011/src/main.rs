use std::collections::HashMap;

fn main() {
    let mut v1: Vec<i32> = Vec::new();
    let _v2 = vec![1, 2, 3];

    v1.push(5);
    v1.push(6);
    v1.push(7);

    // vectors get dropped if out of scope

    let third: &i32 = &v1[2]; //gets a reference
    println!("The third element is {}", third);

    match v1.get(2) {
        //gives us an Option<&T>
        //Some(&element) => println!("The third element could be {}", element),
        Some(third) => println!("The third element is {}", third),
        None => println!("There is no third element"),
    }

    let mut v3 = vec![100, 32, 164];
    for i in &mut v3 {
        *i += 40;
    }

    let row = vec![
        SpreadsheetCell::Int(3),
        SpreadsheetCell::Float(10.12),
        SpreadsheetCell::Text(String::from("blue")),
    ];

    println!("{:?}", row);
    match row.get(1) {
        Some(data) => println!("{:?}", data),
        None => println!("Nothing here"),
    }

    let row_data = &row[2];
    println!("{:?}", row_data);
    if let SpreadsheetCell::Text(data) = row_data {
        println!("value: {}", data)
    }

    // Strings
    let _s = String::new();

    let data = "initial contents";
    let _s = data.to_string();
    let _s = data.to_owned();

    let mut s = String::from("Initial Contents");
    s.push_str(": wwwwoooowww!");

    let s1 = String::from("Hello, ");
    let mut s2 = String::from("world!");
    let s3 = s1 + &mut s2;
    println!("{}", s3);

    let s1 = String::from("tic");
    let s2 = String::from("tac");
    let s3 = String::from("toe");

    let s = format!("{}-{}-{}", s1, s2, s3);
    println!("{}", s);
    println!("{}", s1);

    let hello = "Здравствуйте";
    let _s = &hello[0..4];

    for c in "नमस्ते".chars() {
        println!("{}", c);
    }

    for b in "नमस्ते".bytes() {
        println!("{}", b);
    }

    // HASH MAP
    let mut scores = HashMap::new();
    scores.insert(String::from("Blue"), 10);
    scores.insert(String::from("Yellow"), 50);

    let teams = vec![String::from("Blue"), String::from("Yellow")];
    let inital_scores = vec![10, 50];

    let mut scores: HashMap<_, _> = teams.into_iter().zip(inital_scores.into_iter()).collect();

    let field_name = String::from("Favorite color");
    let field_value = String::from("Blue");

    let mut map = HashMap::new();
    map.insert(field_name, field_value);
    // field_name and field_value are invalid at this point

    let score = scores.get("Blue");
    let team_name = String::from("Blue"); // alternative

    match score {
        Some(score) => println!("value: {}", score),
        None => println!("Loosers"),
    }

    for (key, value) in &scores {
        println!("{}: {}", key, value);
    }

    scores.insert(String::from("Blue"), 25);
    println!("{:?}", scores);

    scores.entry(String::from("Yellow")).or_insert(50); // Only inserting if key has no value

    let text = "hello world wonderful world";

    let mut map = HashMap::new();

    for word in text.split_whitespace() {
        let count = map.entry(word).or_insert(0);
        *count += 1;
    }

    println!("{:?}", map);

    let list_of_integers = vec![1, 2, 3, 4, 5, 6, 7, 12, 15];
    let mut added_numbers = 0;
    for number in &list_of_integers {
        added_numbers += number;
    }
    println!("mean: {}", added_numbers / list_of_integers.len());
}

#[derive(Debug)]
enum SpreadsheetCell {
    Int(i32),
    Float(f64),
    Text(String),
}
