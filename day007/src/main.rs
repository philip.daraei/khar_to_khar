enum IpAddrKind {
    V4,
    V6,
}

struct IpAddr {
    kind: IpAddrKind,
    address: String,
}

fn route(ip_kind: IpAddrKind) {}

#[derive(Debug)]
struct User {
    username: String,
    email: String,
    sign_in: u64,
    active: bool,
}

struct Color(i32, i32, i32);
struct Point(i32, i32, i32);

struct Rectangle {
    width: u32,
    height: u32,
}

impl Rectangle {
    fn area(&self) -> u32 {
        self.width * self.height
    }

    fn can_hold(&self, other: &Rectangle) -> bool {
        self.width > other.width && self.height > other.height
    }

    fn square(size: u32) -> Rectangle {
        Rectangle {
            width: size,
            height: size,
        }
    }
}

fn build_user(email: String, username: String) -> User {
    User {
        email,
        username,
        active: true,
        sign_in: 1,
    }
}

fn main() {
    println!("Hello, world!");

    let mut user1 = User {
        email: String::from("someone@example.com"),
        username: String::from("someusername123"),
        active: true,
        sign_in: 1,
    };

    user1.sign_in = 2;
    let user2 = build_user(
        String::from("someoneelse@example.com"),
        String::from("anotheruser123"),
    );

    println!("{:?}, {:?}", user1, user2);
    let _black = Color(0, 0, 0);
    let _origin = Point(0, 0, 0);

    let four = IpAddrKind::V4;

    route(IpAddrKind::V6);

    let localhost = IpAddr {
        kind: IpAddrKind::V6,
        address: String::from("::1"),
    };
}
