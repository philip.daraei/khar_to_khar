// Variadic functions
func squareVariadic(numbers: Int...) {
    for number in numbers {
        print("\(number) squared is \(number * number).")
    }
}

squareVariadic(numbers: 1, 2, 3, 4, 5)


// Throwing functions
// declare enum first with possible errors
enum PasswordError: Error {
    case obvious
}

func checkPassword(_ password: String) throws -> Bool {
    if password == "password" {
        throw PasswordError.obvious
    }
    return true
}

// Running throwing functions
do {
    try checkPassword("password")
    print("That password is good!")
} catch {
    print("You can't use that password.")
}

// INOUT parameters
func double(number: inout Int) {
    number *= 2
}
// Create integer
var doubleValue = 10
// Pass value as doubleInPlace &
double(number: &doubleValue)
print(doubleValue)

// MARK: - Closures

// Creating basic closures
// Assign funtion to variable
let driving = {
    print("I am driving my car")
}

driving()

// Accepting parameter in a closure
// Keyword `in`
let drivingWithParameter = { (place: String) in
    print("I am going to \(place) in my car")
}
drivingWithParameter("Tehran")

// Returning values from a closure
let drivingWithReturn = { (place: String) -> String in
    return "I am going to \(place) in my car"
}
let message = drivingWithReturn("Tehran")
print(message)

// Closure as parameters
func travel(action: () -> Void) {
    print("I am getting ready to go.")
    action()
    print("I arrived")
}
travel(action: driving)

// Trailing closure syntax
// Condition: if last param is closure
travel {
    driving()
}

// Closures with params
func travelWithParams(action: (String) -> Void) {
    print("I am getting ready to lift off.")
    action("London")
    print("I have landed")
}
travelWithParams { (place: String) in
    print("I am flying to \(place) in my own plane")
}

// Closures with return values
func travelWithReturn(action: (String) -> String) {
    print("I am getting ready to beam.")
    let description = action("Saigon")
    print(description)
    print("Beam completed")
}
travelWithReturn { (place: String) -> String in
    return "I am beaming to \(place) in my particel beam."
}

// Shorthand param names
// Swift knows the param -> drop String
travelWithReturn { place -> String in
    return "I am going to \(place) in my UFO"
}
// It also knows the closure must return a string -> drop return type and return word
travelWithReturn { place in
    "I am heading to \(place) boomshackalaka"
}
// Shorthand syntax -> drop `place in`
travelWithReturn {
    "I am going to \($0) in my train."
}

// Closures with multiple params
func travelWithMultipleParams(action: (String, Int) -> String) {
    print("I am getting ready to travel.")
    let description = action("London", 60)
    print(description)
    print("I arrived again")
}
// Using shorthand syntax again ;)
travelWithMultipleParams {
    "I am going to \($0) at \($1)% of lightspeed."
}

// Returning closures
func travelWithReturnClosure() -> (String) -> Void {
    return {
        print("I am going to \($0).")
    }
}
let result = travelWithReturnClosure()
result("London")

// Capturing values
func eatWithCaptureValue() -> (String) -> Void {
    var counter = 1

    return {
        print("\(counter). I am going to eat \($0).")
        counter += 1
    }
}
let foodToEat = eatWithCaptureValue()
foodToEat("Rice")
foodToEat("Rice")
foodToEat("Rice")
