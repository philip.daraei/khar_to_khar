use std::fmt::Display;

struct Pair<T> {
    x: T,
    y: T,
}

impl<T> Pair<T> {
    fn new(x: T, y: T) -> Self {
        Self { x, y }
    }
}

// Only implement the cmp_display for inner type T if they implement the PartialOrd trait
impl<T: Display + PartialOrd> Pair<T> {
    fn cmp_display(&self) {
        if self.x >= self.y {
            println!("The largest member is x = {}", self.x);
        } else {
            println!("The largest member is y = {}", self.y);
        }
    }
}

/*
// Another example of a blanket implementation
impl<T: Display> ToString for T {
    fn to_string(&self) -> String {
        String::from("foo")
    }
}
*/

fn main() {
    println!("Hello, world!");
    let i32_pair = Pair::new(40, 30);
    i32_pair.cmp_display();

    let char_pair = Pair::new('a', 'b');
    char_pair.cmp_display();

    let string_pair = Pair::new(String::from("foo"), String::from("bar"));
    string_pair.cmp_display();

    let string1 = String::from("adcd");
    let string2 = "xyz";

    let result = longest(string1.as_str(), string2);
    println!("The longest string is {}", result);

    let novel = String::from("Call me Ishmael. Some years ago...");
    let first_sentence = novel.split('.').next().expect("Could not find a '.'");
    let i = ImportantExcerpt {
        part: first_sentence,
    };

    let s: &'static str = "I have a static lifetime";
}

/*
lifetime examples
&i32            a reference
&'a i32         a reference with an explicit lifetime
&'a mut i32     a mutable reference with an explicit lifetime
 */

fn longest<'a>(x: &'a str, y: &'a str) -> &'a str {
    if x.len() > y.len() {
        x
    } else {
        y
    }
}

struct ImportantExcerpt<'a> {
    part: &'a str,
}

impl<'a> ImportantExcerpt<'a> {
    fn announce_and_return_part(&self, announcement: &str) -> &str {
        println!("Attention please {}", announcement);
        self.part
    }
}

// Generic type parameters, trait bounds, and lifetimes

fn longest_with_an_announcement<'a, T>(x: &'a str, y: &'a str, ann: T) -> &'a str
where
    T: Display,
{
    println!("Announcement! {}", ann);
    if x.len() > y.len() {
        x
    } else {
        y
    }
}
