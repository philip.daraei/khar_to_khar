// Import
// import UIKit

// Variable 
var aaa = 5
var bbb:Int = 15

// Constants
let ABCDE = 20
let CONS = "I NEVER CHANGE"

// Different types
var singleLineString = "Hello world!"
var multiLineString = """
    Que passssaaa,
    what's up?
"""
var boolean = true
var abcd:Double

// String interpolation
var verb = "awesome!"
var str = "You are \(verb)"

// Arrays
var beatles = ["John", "Paul", "Ringo", "George"]
let deadBeatles = beatles[0]

// Sets - only unique values - unordered
var colors = Set(["red", "green", "blue"])

// Enums with associated value
enum Activities {
    case running(distance: Int)
    case sholex
}

// Tuples
var name = (first: "Taylor", last: "Swift")
var _ = name.0
var _ = name.first

// Dictionaries
var heights = [
    "Phil": "1.88",
    "everyone else": "1.60 at best"
]
var _ = heights["Phil"]
// default value for non existing entries are possible height["lil": default: "unknown"]

// Creating empty collections
var emptyArray = [Int]()
var emptyDictionary = [Int: String]()

var emptySet = Set<String>()
var alternativeArray = Array<Int>()
var alternativeDic = Dictionary<Int, String>()

let todaysActivity = Activities.running(distance: 6)

// Enum raw values
enum Planet: Int {
    case mars = 0
    case earth
    case venus
    case pluto
}

let earth = Planet(rawValue: 1)!

// Arithmetic operators
// + - * / %

// Compound Operators
// += -+ /= *=

// Comparison Operators
// == != >= <= 

// Operator overloading
let meaningOfLife = 42
let doubleMeaning = meaningOfLife + 42

let firstHalf = ["John", "Paul"]
let secondHalf = ["George", "Ringo"]
let newBeatles = firstHalf + secondHalf

// conditions
var phil = true
var hani = false

for _ in 1...2 {
    if phil {
        print("you are cool 😎")
        phil = false
    } else if !hani {
        print("okay okay you are cool too 😎")
    } else {
        print("you not so cool")
    }
}
phil = true

// combining operators
let age1 = 11
let age2 = 12

if age1 > 18 && age2 > 18 {
    print("Both are over 18")
}

if phil || hani {
    print("at least one cool person 😎")
}

// Tenary operator
print(phil ? "Damn boy, you true!" : "Damn boy you false")

// Switch cases
let weather = "rain"

switch weather {
case "sunny":
    print("Wear suncream")
case "snow":
    print("Wrap up warm")
case "rain":
    print("Dance in the rain")
    fallthrough
default:
    print("Enjoy your day")
}

// range operators
// half open range operator
let halfOpen = 1..<5 // 1,2,3,4

// closed range operator
let closedRange = 1...5 //1,2,3,4,5

// MARK: - LOOPS
// For loop
for item in halfOpen {
    print("\(item)")
}

print("Players gonna ")

for _ in closedRange {
    print("play")
}

// While loop
var number = 1

while number <= 5 {
    print(number)
    number += 1
}

// Repeat loops - always executed ones
var numberB = 1

repeat {
    print(numberB)
    numberB += 1
} while numberB <= 5

repeat {
    print("This is false")
} while false

// Exiting loops
var countDown = 10

while countDown >= 0 {
    print(countDown)

    if countDown == 4 {
        print("I am bored, let's go now!")
        break
    }
    countDown -= 1
}

// Exiting multiple loops - use LABEL (e.g. outerLoop)
outerLoop: for i in 1...10 {
    for j in 1...10 {
        let product = i * j
        print("\(i) * \(j) is \(product)")

        if product == 50 {
            print("It's a bullseye!")
            break outerLoop
        }
    }
}

// Skipping items
for k in 1...10 {
    if k % 2 == 1 {
        continue
    }

    print(k)
}

// Infinite loops
var counter = 0

while true {
    print(" ")
    counter += 1

    if counter == 7 {
        break
    }
}

// MARK: - FUNCTIONS SYNTAX

// Writing functions 
func printHello() {
    let message = """
Welcome to MyApp!

Run this app inside a directory of images and
MyApp will resize them all into thumbnails
"""

print(message)
}

printHello()

// Accepting parameters
func square(number: Int) {
    print(number * number)
}

square(number: 6)

// Returning values
func squareWithReturn(number: Int) -> Int {
    return number * number
}

let returnValue = squareWithReturn(number: 7)

// Parameter labels
func sayHelloTo(to name: String) {
    print("Hello, \(name)!")
}

sayHelloTo(to: "Dieter")

// Omitting parameter labels
func greet(_ name: String) {
    print("Hello, \(name)!")
}

greet("Holger")

// default parameters
func greetDefault(_ person: String, nicely: Bool = true) {
    if nicely {
        print("Hello, \(person)!")
    } else {
        print("Oh no, it's \(person) again!")
    }
}

greetDefault("Taylor")
greetDefault("Taylor", nicely: false)

// Variadic functions
func squareVariadic(numbers: Int...) {
    for number in numbers {
        print("\(number) squared is \(number * number).")
    }
}

squareVariadic(numbers: 1, 2, 3, 4, 5)


// Throwing functions
// declare enum first with possible errors
enum PasswordError: Error {
    case obvious
}

func checkPassword(_ password: String) throws -> Bool {
    if password == "password" {
        throw PasswordError.obvious
    }
    return true
}

// Running throwing functions
do {
    try checkPassword("password")
    print("That password is good!")
} catch {
    print("You can't use that password.")
}

// INOUT parameters
func double(number: inout Int) {
    number *= 2
}
// Create integer
var doubleValue = 10
// Pass value as doubleInPlace &
double(number: &doubleValue)
print(doubleValue)
